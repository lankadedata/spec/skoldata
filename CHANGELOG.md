    # Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/sv/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Lägger till exempel på datamängd i CSV-, ODS- och XLSX-format.

## [1.1.0] - 2018-12-11
### Added
- Sammanfattande kapitel om CSV formatet.
- Schema / tabell metadata för CSV i separat fil.
- Introducerade kapitel om JSON format.
- Introducerade kapitel om uttryck i DCAT.
- Introducerade appendix A och B med exempel.

### Changed
- Flyttade över specifikationen till en ny mall.
- Flyttade upp och renodlade kapitel 2 om datamodell.

## [1.0.5] - 2018-11-22
### Added
- La till permanent adress till specifikationen.

### Changed
- Översatte rubriken på versionshistorik till svenska.
- Ändrade kapitalisering i kolumn 4 från GrundSkola till GRundskola så det blir tydligt vad koden ‘GR’ står för.

## [1.0.0] - 2017-03-19
### Added
- Slut-granskning gjord.
- Publicering av Lunds kommun gjord enligt specifikation.

## [0.9.8] - 2016-11-17
### Added
- Fler kolumner baserat på exempel från Haninge.

### Changed
- Separation av modell och format.
- Förtydling av förväntade värden per kolumn.
- Tabell annotering enligt CSV on the web för validering och konvertering till RDF.

## [0.1.0] - 2016-10-21
### Added
- Utkast av Magnus Burell på Hemnet
- Förslag på CSV uttryck.
- Förslag på kolumner från tidigare inhämtad data.

[Unreleased]: 
